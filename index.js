let posts = [];
let count = 1;

// Add movie post data

document.querySelector('#form-add-post').addEventListener('submit', (event) => {
	event.preventDefault();

	posts.push({
		id: count,
		title: document.querySelector('#txt-title').value,
		body: document.querySelector('#txt-body').value
	})

	count++;

	showPosts(posts);
	alert('Successfully added the movie post.');
});

// Show movie posts
const showPosts = (posts) => {
	let postEntries = ''; // it acts as a container

	posts.forEach((post) => {
		postEntries += `
			<div id='post-${post.id}'>
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')" id="post-delete-${post.id}">Delete</button>
			</div>
		`;
	})

	document.querySelector('#div-post-entries').innerHTML = postEntries;
}
// When we click a button, it refreshes the page and makes the posts array empty again..so, we will just get a glimpse of it for half second maybe..

// -------------S49 discussion start--------------

// Edit movie post
const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value= id
	document.querySelector('#txt-edit-title').value= title
	document.querySelector('#txt-edit-body').value= body
}

// Update movie post
	document.querySelector("#form-edit-post").addEventListener('submit', (event) => {
		event.preventDefault();

		for(let i=0; i < posts.length; i++){
			if(posts[i].id.toString() === document.querySelector('#txt-edit-id').value){
				posts[i].title = document.querySelector('#txt-edit-title').value;
				posts[i].body = document.querySelector('#txt-edit-body').value;

				showPosts(posts);
				alert('Successfully updated our movie post');

				break;
			}
		}
	})

// -----------------s49 activity start-------------------------

// Delete movie post
const deletePost = (id) => {
	posts = posts.filter((post) => {
		if(post.id.toString() !== id){
			return post;
		}
	});

	document.querySelector(`#post-${id}`).remove();
}